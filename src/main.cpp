#include <mutex>

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>

#include "KinematicsModel.hpp"

#include "generalized_information_filter/residuals/landmark-prediction.h"
#include "generalized_information_filter/residuals/landmark-prediction-augmented.h"
#include "generalized_information_filter/residuals/leg-kinematic-update.h"
#include "generalized_information_filter/residuals/leg-dynamic-residual.h"
#include "generalized_information_filter/residuals/pose-update.h"
#include "generalized_information_filter/residuals/random-walk-prediction.h"
#include "generalized_information_filter/residuals/robcen-landmark-findif-residual.h"
#include "generalized_information_filter/residuals/robcen-imuacc-findif-residual.h"
#include "generalized_information_filter/residuals/robcen-velocity-findif-residual.h"
#include "generalized_information_filter/residuals/robcen-rotrate-findif-residual.h"
#include "generalized_information_filter/residuals/robcen-imuror-update.h"
#include "generalized_information_filter/filter.h"

class ImuKinFilter: public GIF::Filter{
 public:
  typedef GIF::RandomWalkPrediction<GIF::ElementPack<GIF::Vec3,GIF::Vec3>> ImuBiasPrediction;
  typedef GIF::LegKinematicUpdate<KinematicsModel> KinUpdate;
  typedef GIF::RobcenLandmarkFindifResidual<KinematicsModel::kNumLeg> RobcenFootholdFindifResidual;
  typedef GIF::RobcenImuaccFindifResidual RobcenImuaccFindifResidual;
  typedef GIF::RobcenVelocityFindifResidual RobcenVelocityFindifResidual;
  typedef GIF::RobcenRotrateFindifResidual RobcenRotrateFindifResidual;
  typedef GIF::RobcenImurorUpdate RobcenImurorUpdate;
  typedef GIF::PoseUpdate PoseUpdate;
  typedef GIF::RandomWalkPrediction<GIF::ElementPack<GIF::Vec3,GIF::Quat>> InertialTransformPrediction;
  typedef GIF::LegDynamicResidual<KinematicsModel> DynResidual;
  typedef GIF::RandomWalkPrediction<GIF::ElementPack<double>> MassPrediction;
  typedef GIF::RandomWalkPrediction<GIF::ElementPack<GIF::Vec3>> MassOffsetPrediction;
  std::shared_ptr<ImuBiasPrediction> imuBiasPrediction_;
  std::shared_ptr<RobcenFootholdFindifResidual> robcenFootholdFindifResidual_;
  std::shared_ptr<KinUpdate> legKinematicUpdate_;
  std::shared_ptr<RobcenImuaccFindifResidual> robcenImuaccFindifResidual_;
  std::shared_ptr<RobcenVelocityFindifResidual> robcenVelocityFindifResidual_;
  std::shared_ptr<RobcenRotrateFindifResidual> robcenRotrateFindifResidual_;
  std::shared_ptr<RobcenImurorUpdate> robcenImurorUpdate_;
  std::shared_ptr<PoseUpdate> poseUpdate_;
  std::shared_ptr<InertialTransformPrediction> inertialTransformPrediction_;
  std::shared_ptr<DynResidual> legDynamicResidual_;
  std::shared_ptr<MassPrediction> massPrediction_;
  std::shared_ptr<MassOffsetPrediction> massOffsetPrediction_;
  int imu_bias_prediction_id_;
  int robcen_foothold_findif_residual_id_;
  int kinematic_update_id_;
  int robcen_imuacc_findif_residual_id_;
  int robcen_velocity_findif_residual_id_;
  int robcen_rotrate_findif_residual_id_;
  int robcen_imuror_update_id_;
  int pose_update_id_;
  int inertial_transform_prediction_;
  int dynamic_residual_id_;
  int mass_prediction_id_;
  int mass_offset_prediction_id_;
  std::array<bool,4> contacts_prev_;

  // Covariances
  const double IrIM_pre = 1e-6;        //
  const double MvM_pre = 1e-5;         // IMU acc
  const double MwM_bias_pre = 1e-8;    // IMU gyr bias
  const double MfM_bias_pre = 1e-8;    // IMU acc bias
  const double qIM_pre = 1e-6;         //
  const double MrML_pre = 1e-6;
  const double IrIJ_pre = 1e-8;
  const double qIJ_pre = 1e-8;
  const double MwM_pre = 1e-6;         // IMU gyr
  const double m_pre = 1e-2;
  const double mo_pre = 1e-6;
  const double kin_upd = 1e-2;
  const double JrJC_upd = 1e-4;
  const double qJC_upd = 1e-4;
  const double dyn_upd = 1e3;          // dyn
  const double IrIM_init = 1e-8;
  const double MvM_init = 1e-2;
  const double MwM_bias_init = 1e-2;
  const double MfM_bias_init = 1e-2;
  const double qIM_init = 1e-2;
  const double MrML_init = 1e-2;
  const double IrIJ_init = 1e-8;
  const double qIJ_init = 1e-4;
  const double m_init = 100;
  const double mo_init = 1e-2;
  const bool useExtPose = false;
  const bool useDynamics = true;

  ImuKinFilter(const std::shared_ptr<KinematicsModel>& model):
    imuBiasPrediction_(new ImuBiasPrediction("ImuBiasPrediction", {"MwM_bias", "MfM_bias"}, {"MwM_bias", "MfM_bias"})),
    robcenFootholdFindifResidual_(new RobcenFootholdFindifResidual("RobcenFootholdFindifResidual",
      {"MrML"}, {"MrML", "MvM"}, {"MrML", "MwM"}, {"MrML"})),
    robcenImuaccFindifResidual_(new RobcenImuaccFindifResidual("RobcenImuaccFindifResidual",
      {"MvM"}, {"MvM", "MfM_bias", "qIM"}, {"MvM", "MwM"}, {"MvM"})),
    robcenVelocityFindifResidual_(new RobcenVelocityFindifResidual("RobcenVelocityFindifResidual",
      {"IrIM"}, {"IrIM", "MvM", "qIM"}, {"IrIM"}, {"IrIM"})),
    robcenRotrateFindifResidual_(new RobcenRotrateFindifResidual("RobcenRotrateFindifResidual",
      {"qIM"}, {"qIM"}, {"qIM", "MwM"}, {"qIM"})),
    robcenImurorUpdate_(new RobcenImurorUpdate("RobcenImurorUpdate",
      {"MwM"}, {}, {"MwM", "MwM_bias"}, {"MwM"})),
    legKinematicUpdate_(new KinUpdate("KinematicUpdate","BrBL","MrML","BrBL")),
    poseUpdate_(new PoseUpdate("PoseUpdate",
                               {"JrJC", "qJC"}, {"IrIM", "qIM", "IrIJ", "qIJ"},{"JrJC", "qJC"})),
    inertialTransformPrediction_(new InertialTransformPrediction("InertialTransformPrediction",
                                                                 {"IrIJ","qIJ"},{"IrIJ", "qIJ"})),
    legDynamicResidual_(new DynResidual("LegDynamicResidual","dyn",{"MvM", "MwM", "qIM", "m", "mo"},
                                          {"MvM", "MwM"},"dyn")),
    massPrediction_(new MassPrediction("MassPrediction",{"m"},{"m"})),
    massOffsetPrediction_(new MassOffsetPrediction("MassOffsetPrediction",{"mo"},{"mo"}))
  {
    imu_bias_prediction_id_ = -1;
    robcen_foothold_findif_residual_id_ = -1;
    kinematic_update_id_ = -1;
    robcen_imuacc_findif_residual_id_ = -1;
    robcen_velocity_findif_residual_id_ = -1;
    robcen_rotrate_findif_residual_id_ = -1;
    robcen_imuror_update_id_ = -1;
    pose_update_id_ = -1;
    inertial_transform_prediction_ = -1;
    dynamic_residual_id_ = -1;
    mass_prediction_id_ = -1;
    mass_offset_prediction_id_ = -1;

    // Imu Bias Prediction Properties
    imuBiasPrediction_->GetNoiseCovarianceBlock("MwM_bias") = GIF::Mat3::Identity()*MwM_bias_pre;
    imuBiasPrediction_->GetNoiseCovarianceBlock("MfM_bias") = GIF::Mat3::Identity()*MfM_bias_pre;
    imu_bias_prediction_id_ = AddResidual(imuBiasPrediction_, GIF::fromSec(10.0), GIF::fromSec(0.0));

    // Kinematic Update Properties
    legKinematicUpdate_->SetModelPtr(model);
    legKinematicUpdate_->SetExtrinsics(GIF::Vec3(-0.0959,-0.0060,-0.0055),
                                       GIF::Quat(0,0,0,1));
    legKinematicUpdate_->GetNoiseCovarianceBlock("BrBL") = GIF::Mat<12>::Identity()*kin_upd;
    kinematic_update_id_ = AddResidual(legKinematicUpdate_, GIF::fromSec(0.1), GIF::fromSec(0.0));

    robcenFootholdFindifResidual_->GetNoiseCovarianceBlock("MrML") = GIF::Mat<12>::Identity()*MrML_pre;
    robcen_foothold_findif_residual_id_ = AddResidual(robcenFootholdFindifResidual_,
                                                      GIF::fromSec(10.0), GIF::fromSec(0.0));

    robcenImuaccFindifResidual_->GetNoiseCovarianceBlock("MvM") = GIF::Mat3::Identity()*MvM_pre;
    robcen_imuacc_findif_residual_id_ = AddResidual(robcenImuaccFindifResidual_,
                                                    GIF::fromSec(10.0), GIF::fromSec(0.0));

    robcenVelocityFindifResidual_->GetNoiseCovarianceBlock("IrIM") = GIF::Mat3::Identity()*IrIM_pre;
    robcen_velocity_findif_residual_id_ = AddResidual(robcenVelocityFindifResidual_,
                                                    GIF::fromSec(10.0), GIF::fromSec(0.0));

    robcenRotrateFindifResidual_->GetNoiseCovarianceBlock("qIM") = GIF::Mat3::Identity()*qIM_pre;
    robcen_rotrate_findif_residual_id_ = AddResidual(robcenRotrateFindifResidual_,
                                                    GIF::fromSec(10.0), GIF::fromSec(0.0));

    robcenImurorUpdate_->GetNoiseCovarianceBlock("MwM") = GIF::Mat3::Identity()*MwM_pre;
    robcen_imuror_update_id_ = AddResidual(robcenImurorUpdate_,
                                                    GIF::fromSec(10.0), GIF::fromSec(0.0));



    if(useExtPose){
      // Pose Update Properties
      poseUpdate_->SetExtrinsics(GIF::Vec3(-0.00767945749948, -0.0517978804031, 0.186153163019),
                                 GIF::Quat(0.934054240672, 0.00939820305878, -0.00866956828012, -0.356902210482)); // Second (better noise tuning)
      poseUpdate_->GetNoiseCovarianceBlock("JrJC") = GIF::Mat3::Identity()*JrJC_upd;
      poseUpdate_->GetNoiseCovarianceBlock("qJC") = GIF::Mat3::Identity()*qJC_upd;
      pose_update_id_ = AddResidual(poseUpdate_, GIF::fromSec(0.1),GIF::fromSec(0.0));

      // Inertial Transform Prediction Properties
      inertialTransformPrediction_->GetNoiseCovarianceBlock("IrIJ") = GIF::Mat3::Identity()*IrIJ_pre;
      inertialTransformPrediction_->GetNoiseCovarianceBlock("qIJ") = GIF::Mat3::Identity()*qIJ_pre;
      inertial_transform_prediction_ = AddResidual(inertialTransformPrediction_,
                                                   GIF::fromSec(10.0),GIF::fromSec(0.0));
    }

    if(useDynamics){
      // Dynamic Residial Properties
      legDynamicResidual_->SetModelPtr(model);
      legDynamicResidual_->GetNoiseCovarianceBlock("dyn") = GIF::Mat<18>::Identity()*dyn_upd;
      legDynamicResidual_->SetExtrinsics(GIF::Vec3(-0.0959,-0.0060,-0.0055), GIF::Quat(0,0,0,1));
      dynamic_residual_id_ = AddResidual(legDynamicResidual_, GIF::fromSec(10.0), GIF::fromSec(0.0));

      massPrediction_->GetNoiseCovarianceBlock("m") = GIF::Mat<1>::Identity()*m_pre;
      mass_prediction_id_ = AddResidual(massPrediction_,  GIF::fromSec(10.0),GIF::fromSec(0.0));

      massOffsetPrediction_->GetNoiseCovarianceBlock("mo") = GIF::Mat<1>::Identity()*mo_pre;
      mass_offset_prediction_id_ = AddResidual(massOffsetPrediction_,  GIF::fromSec(10.0),GIF::fromSec(0.0));
    }
    std::cout << PrintConnectivity();

    // Set contacts to false
    for(int i=0;i<4;i++){
      contacts_prev_[i] = false;
    }
  }
  virtual ~ImuKinFilter(){};
  void PreProcess(){
    if(robcen_foothold_findif_residual_id_ >= 0){
      for(int i=0;i<4;i++){
        if(!contacts_prev_[i] && legKinematicUpdate_->GetContactFlagFromMeas(i)){
          std::cout << "New contact " << i << std::endl;
          robcenFootholdFindifResidual_->SetPropagationFlag(i,false);
          const GIF::Vec3 MrML = legKinematicUpdate_->GetMrMFFromMeas(i);
          GetLinState().GetValue<std::array<GIF::Vec3,KinematicsModel::kNumLeg>>("MrML")[i] = MrML;
        } else {
          robcenFootholdFindifResidual_->SetPropagationFlag(i,true);
        }
      }
    }
//    testDynamics();
  };
  void PostProcess(){
    for(int i=0;i<4;i++){
      contacts_prev_[i] = legKinematicUpdate_->GetContactFlagFromMeas(i);
    }
    if(dynamic_residual_id_ >= 0){
      if(residuals_.at(dynamic_residual_id_).isActive_){
        legDynamicResidual_->SetPreviousEncoderData();
      }
    }
  };
  void Init(const GIF::TimePoint& t = GIF::TimePoint::min(), const GIF::ElementVectorBase* initState = nullptr) {
    startTime_ = t;
    time_ = t;
    state_.GetValue<double>("m") = 20;
    state_.GetValue<GIF::Vec3>("mo").setZero();
    inf_.setIdentity();
    GetNoiseInfBlock("IrIM") = GIF::Mat3::Identity()/IrIM_init;
    GetNoiseInfBlock("MvM") = GIF::Mat3::Identity()/MvM_init;
    GetNoiseInfBlock("MwM_bias") = GIF::Mat3::Identity()/MwM_bias_init;
    GetNoiseInfBlock("MfM_bias") = GIF::Mat3::Identity()/MfM_bias_init;
    GetNoiseInfBlock("qIM") = GIF::Mat3::Identity()/qIM_init;
    if(robcen_foothold_findif_residual_id_ > 0 || kinematic_update_id_ >= 0){
      GetNoiseInfBlock("MrML") = GIF::Mat<12>::Identity()/MrML_init;
    }
    if(useExtPose){
      GetNoiseInfBlock("IrIJ") = GIF::Mat3::Identity()/IrIJ_init;
      GetNoiseInfBlock("qIJ") = GIF::Mat3::Identity()/qIJ_init;
    }
    if(useDynamics){
      GetNoiseInfBlock("m") = GIF::Mat<1>::Identity()/m_init;
      GetNoiseInfBlock("mo") = GIF::Mat3::Identity()/mo_init;
    }

    // Temporary hack
    is_initialized_ = true;
    LOG(INFO) << "Initializing state at t = " << GIF::Print(t) << std::endl;
    if(dynamic_residual_id_ >= 0){
      legDynamicResidual_->ResetPreviousEncoderData();
    }
    return;

    // Use accelerometer to estimate initial attitude
    GIF::Vec3 unitZ(0,0,1);
    std::shared_ptr<const GIF::ElementVectorBase> accMeas;
    std::shared_ptr<const GIF::ElementVectorBase> poseMeas;
    if(residuals_.at(robcen_imuacc_findif_residual_id_).mt_.GetFirst(accMeas)
       && (!useExtPose || residuals_.at(pose_update_id_).mt_.GetFirst(poseMeas))){
      const GIF::Vec3 MfM = std::dynamic_pointer_cast<const GIF::AccMeas>(accMeas)->MfM_;
      // Align with gravity
      if(MfM.norm()>1e-6){
        GIF::Quat q;
        q.setFromVectors(state_.GetValue<GIF::Quat>("qIM").rotate(MfM),unitZ);
        state_.GetValue<GIF::Quat>("qIM") = q.inverted()*state_.GetValue<GIF::Quat>("qIM");
      }
      // Align external pose frame
      if(useExtPose){
        const GIF::Vec3 JrJC = std::dynamic_pointer_cast<const GIF::PoseMeas>(poseMeas)->JrJC_;
        const GIF::Quat qJC = std::dynamic_pointer_cast<const GIF::PoseMeas>(poseMeas)->qJC_;
        GIF::Vec3 MrMC;
        GIF::Quat qMC;
        poseUpdate_->GetExtrinsics(&MrMC,&qMC);
        state_.GetValue<GIF::Quat>("qIJ") = state_.GetValue<GIF::Quat>("qIM") * qMC * qJC.inverted();
        state_.GetValue<GIF::Vec3>("IrIJ") = state_.GetValue<GIF::Vec3>("IrIM")
                                             + state_.GetValue<GIF::Quat>("qIM").rotate(MrMC)
                                             - state_.GetValue<GIF::Quat>("qIJ").rotate(JrJC);
      }
      LOG(INFO) << "Initializing state at t = " << GIF::Print(t) << std::endl;
      if(dynamic_residual_id_ >= 0){
        legDynamicResidual_->ResetPreviousEncoderData();
      }
      is_initialized_ = true;
    }

  }

  void testDynamics(){
    // Init state
    int kDof = DynResidual::kDof_;
    DynResidual::DynRes dyn_inn;
    GIF::Vec3 MvM_pre;
    GIF::Vec3 MwM_prev;
    GIF::Quat qIM_pre;
    double m = state_.GetValue<double>("m");
    GIF::Vec3 mo = state_.GetValue<GIF::Vec3>("mo");
    GIF::Vec3 MvM_cur;
    GIF::Vec3 MwM_cur;
    DynResidual::DynRes dyn_noi;
    MvM_pre.setZero();
    MwM_prev.setZero();
    qIM_pre = state_.GetValue<GIF::Quat>("qIM");
    MvM_cur.setZero();
    MwM_cur.setZero();
    dyn_noi.setZero();
    GIF::MatX J(kDof,state_.GetDim());
    double d = 1e-4;
    GIF::Vec3 dq(2*d,0,0);
    GIF::Vec3 dv(2*d,0,0);
    GIF::Vec3 dw(2*d,0,0);
    Eigen::Matrix<double,1,1> dm(2*d);
    GIF::Vec3 dmo(2*d,0,0);
    Eigen::Matrix<double,13,1> dx;
    dx.setZero();
    dx(0) = 2*d;

    // Eval residual and Jacobian
    std::cout << "-----------------------------------------------------------------------" << std::endl;
    std::cout << qIM_pre << std::endl;
    std::cout << MvM_pre.transpose() << std::endl;
    std::cout << MwM_prev.transpose() << std::endl;
    for(int i=0;i<50 && dx.norm() > d; i++){
      legDynamicResidual_->verbose_ = false;
      legDynamicResidual_->Eval(dyn_inn,MvM_pre,MwM_prev,qIM_pre,m,mo,MvM_cur,MwM_cur,dyn_noi);
      std::cout << dyn_inn.norm() << std::endl;

      legDynamicResidual_->JacPre(J,MvM_pre,MwM_prev,qIM_pre,m,mo,MvM_cur,MwM_cur,dyn_noi);

//      GIF::MatX Jsub = J.block(0,legDynamicResidual_->PreDefinition()->GetStart(legDynamicResidual_->PreDefinition()->FindName("qIM")),kDof,3);

      dx = J.colPivHouseholderQr().solve(-dyn_inn);
      dq = dx.segment(legDynamicResidual_->PreDefinition()->GetStart(legDynamicResidual_->PreDefinition()->FindName("qIM")),3);
      dv = dx.segment(legDynamicResidual_->PreDefinition()->GetStart(legDynamicResidual_->PreDefinition()->FindName("MvM")),3);
      dw = dx.segment(legDynamicResidual_->PreDefinition()->GetStart(legDynamicResidual_->PreDefinition()->FindName("MwM")),3);
      dm = dx.segment(legDynamicResidual_->PreDefinition()->GetStart(legDynamicResidual_->PreDefinition()->FindName("m")),1);
      dmo = dx.segment(legDynamicResidual_->PreDefinition()->GetStart(legDynamicResidual_->PreDefinition()->FindName("mo")),3);

      qIM_pre = qIM_pre.boxPlus(dq);
      MvM_pre += dv;
      MwM_prev += dw;
      m += dm(0);
      mo += dmo;
      std::cout << qIM_pre << std::endl;
      std::cout << MvM_pre.transpose() << std::endl;
      std::cout << MwM_prev.transpose() << std::endl;
    }
  }
};


class DynamicStateEstimatorNode{
 public:
  ros::NodeHandle nh_;
  ros::Publisher pubOdometry_;
  ros::Subscriber subImu_;
  ros::Subscriber subJointState_;
  ros::Subscriber subContactForceLfFoot_;
  ros::Subscriber subContactForceRfFoot_;
  ros::Subscriber subContactForceLhFoot_;
  ros::Subscriber subContactForceRhFoot_;
  ros::Subscriber subPose_;

  nav_msgs::Odometry odometryMsg_;
  int msgSeq_;

  std::shared_ptr<KinematicsModel> model_;
  ImuKinFilter filter_;

  std::mutex mutex_;
  double contact_force_threshold_;
  bool contacts_[4];

  DynamicStateEstimatorNode(ros::NodeHandle& nh): nh_(nh),
      model_(new KinematicsModel()), filter_(model_){

    FLAGS_minloglevel = 0;
#ifndef NDEBUG
    ROS_WARN("====================== Debug Mode ======================");
#endif

    contact_force_threshold_ = 1000;

    // Get URDF string
    std::string quadrupedUrdfDescription;
    nh_.param<std::string>("/quadruped_description", quadrupedUrdfDescription, "");

    /* initialize model from URDF decription */
    if(!model_->initModelFromUrdfString(quadrupedUrdfDescription)) {
      LOG(ERROR) << "Could not initialize quadruped model from urdf description!";
    }

    // Set model state to random
    quadruped_model::QuadrupedState state_;
    state_.setRandom();
    model_->setState(state_);

    // Test filter
    filter_.TestJacs(1e-6,1e-6);

    // Subscribe topics
    subImu_ = nh_.subscribe("imu0", 1000, &DynamicStateEstimatorNode::imuCallback,this);
    subJointState_ = nh_.subscribe("joint_states", 1000,
                                   &DynamicStateEstimatorNode::jointCallback,this);
    subContactForceLfFoot_ = nh_.subscribe("contact_force_lf_foot", 1000,
                                           &DynamicStateEstimatorNode::forceCallbackLf,this);
    subContactForceRfFoot_ = nh_.subscribe("contact_force_rf_foot", 1000,
                                           &DynamicStateEstimatorNode::forceCallbackRf,this);
    subContactForceLhFoot_ = nh_.subscribe("contact_force_lh_foot", 1000,
                                           &DynamicStateEstimatorNode::forceCallbackLh,this);
    subContactForceRhFoot_ = nh_.subscribe("contact_force_rh_foot", 1000,
                                           &DynamicStateEstimatorNode::forceCallbackRh,this);
    subPose_ = nh_.subscribe("external_pose", 1000, &DynamicStateEstimatorNode::poseCallback,this);

    // Publishers
    pubOdometry_ = nh_.advertise<nav_msgs::Odometry>("odometry", 1);
    odometryMsg_.header.frame_id = "world";
    odometryMsg_.child_frame_id = "imu";
    msgSeq_ = 1;

    // Init contacts
    for(int i=0;i<4;i++){
      contacts_[i] = false;
    }
  }
  ~DynamicStateEstimatorNode(){}

  void imuCallback(const sensor_msgs::Imu::ConstPtr& msg){
    std::lock_guard<std::mutex> lock(mutex_);
    std::shared_ptr<GIF::AccMeas> accMeas(new GIF::AccMeas(
        Eigen::Vector3d(msg->linear_acceleration.x,
                        msg->linear_acceleration.y,
                        msg->linear_acceleration.z)));
    std::shared_ptr<GIF::RorMeas> rorMeas(new GIF::RorMeas(
        Eigen::Vector3d(msg->angular_velocity.x,
                        msg->angular_velocity.y,
                        msg->angular_velocity.z)));
    std::shared_ptr<GIF::EmptyMeas> emptyMeas(new GIF::EmptyMeas());
    if(filter_.robcen_imuacc_findif_residual_id_ >= 0)
      filter_.AddMeasurement(filter_.robcen_imuacc_findif_residual_id_,accMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    if(filter_.robcen_imuror_update_id_ >= 0)
      filter_.AddMeasurement(filter_.robcen_imuror_update_id_,rorMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    if(filter_.robcen_rotrate_findif_residual_id_ >= 0)
      filter_.AddMeasurement(filter_.robcen_rotrate_findif_residual_id_,emptyMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    if(filter_.robcen_velocity_findif_residual_id_ >= 0)
      filter_.AddMeasurement(filter_.robcen_velocity_findif_residual_id_,emptyMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    if(filter_.robcen_foothold_findif_residual_id_ >= 0)
      filter_.AddMeasurement(filter_.robcen_foothold_findif_residual_id_,emptyMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    if(filter_.imu_bias_prediction_id_ >= 0)
      filter_.AddMeasurement(filter_.imu_bias_prediction_id_,emptyMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    if(filter_.mass_prediction_id_ >= 0)
      filter_.AddMeasurement(filter_.mass_prediction_id_,emptyMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    if(filter_.mass_offset_prediction_id_ >= 0)
      filter_.AddMeasurement(filter_.mass_offset_prediction_id_,emptyMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
    Update();
  }

  void jointCallback(const sensor_msgs::JointState::ConstPtr& msg){
    std::lock_guard<std::mutex> lock(mutex_);

    std::shared_ptr<GIF::KinematicMeasurement<4,3>> kinMeas(new GIF::KinematicMeasurement<4,3>());
    for(int i=0; i<4; i++){
      kinMeas->contact_flag_[i] = contacts_[i];
      kinMeas->kin_[i] = Eigen::Vector3d(msg->position[3*i+0],
                                         msg->position[3*i+1],
                                         msg->position[3*i+2]);
    }
    if(filter_.kinematic_update_id_ >= 0)
      filter_.AddMeasurement(filter_.kinematic_update_id_,kinMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));

    std::shared_ptr<GIF::DynamicMeasurement<4,3>> dynMeas(new GIF::DynamicMeasurement<4,3>());
    for(int i=0; i<4; i++){
      dynMeas->contact_flag_[i] = contacts_[i];
      dynMeas->enc_[i] = Eigen::Vector3d(msg->position[3*i+0],
                                         msg->position[3*i+1],
                                         msg->position[3*i+2]);
      dynMeas->end_[i] = Eigen::Vector3d(msg->velocity[3*i+0],
                                         msg->velocity[3*i+1],
                                         msg->velocity[3*i+2]);
      dynMeas->dyn_[i] = Eigen::Vector3d(msg->effort[3*i+0],
                                         msg->effort[3*i+1],
                                         msg->effort[3*i+2]);
    }
    if(filter_.dynamic_residual_id_ >= 0)
      filter_.AddMeasurement(filter_.dynamic_residual_id_,dynMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));

    Update();
  }

  void forceCallbackLf(const geometry_msgs::WrenchStamped::ConstPtr& msg){
    std::lock_guard<std::mutex> lock(mutex_);
    contacts_[0] = getContactStateFromContactForce(msg);
  }

  void forceCallbackRf(const geometry_msgs::WrenchStamped::ConstPtr& msg){
    std::lock_guard<std::mutex> lock(mutex_);
    contacts_[1] = getContactStateFromContactForce(msg);
  }

  void forceCallbackLh(const geometry_msgs::WrenchStamped::ConstPtr& msg){
    std::lock_guard<std::mutex> lock(mutex_);
    contacts_[2] = getContactStateFromContactForce(msg);
  }

  void forceCallbackRh(const geometry_msgs::WrenchStamped::ConstPtr& msg){
    std::lock_guard<std::mutex> lock(mutex_);
    contacts_[3] = getContactStateFromContactForce(msg);
  }

  bool getContactStateFromContactForce(const geometry_msgs::WrenchStamped::ConstPtr& msg) const{
    return GIF::Vec3(msg->wrench.force.x, msg->wrench.force.y,msg->wrench.force.z).norm()
        > contact_force_threshold_; // TODO: improve
  }

  void poseCallback(const geometry_msgs::TransformStamped::ConstPtr& msg){
    if(filter_.useExtPose){
      std::lock_guard<std::mutex> lock(mutex_);
      std::shared_ptr<GIF::PoseMeas> poseMeas(new GIF::PoseMeas(
          GIF::Vec3(msg->transform.translation.x,
                    msg->transform.translation.y,
                    msg->transform.translation.z),
          GIF::Quat(msg->transform.rotation.w,
                    msg->transform.rotation.x,
                    msg->transform.rotation.y,
                    msg->transform.rotation.z)));
      filter_.AddMeasurement(filter_.pose_update_id_,poseMeas,
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
      filter_.AddMeasurement(filter_.inertial_transform_prediction_,std::make_shared<GIF::EmptyMeas>(),
                             GIF::TimePoint(GIF::fromSec(msg->header.stamp.toSec())));
      Update();
    }
  }

  void Update(){
    filter_.Update();
    if(filter_.IsInitialized()){
      std::cout << filter_.GetState().GetValue<double>("m") << std::endl;
      std::cout << filter_.GetState().GetValue<GIF::Vec3>("mo").transpose() << std::endl;
      odometryMsg_.header.seq = msgSeq_;
      odometryMsg_.header.stamp = ros::Time(GIF::toSec(filter_.GetTime().time_since_epoch()));

      odometryMsg_.pose.pose.position.x = filter_.GetState().GetValue<GIF::Vec3>("IrIM")(0);
      odometryMsg_.pose.pose.position.y = filter_.GetState().GetValue<GIF::Vec3>("IrIM")(1);
      odometryMsg_.pose.pose.position.z = filter_.GetState().GetValue<GIF::Vec3>("IrIM")(2);
      odometryMsg_.pose.pose.orientation.w = filter_.GetState().GetValue<GIF::Quat>("qIM").w();
      odometryMsg_.pose.pose.orientation.x = filter_.GetState().GetValue<GIF::Quat>("qIM").x();
      odometryMsg_.pose.pose.orientation.y = filter_.GetState().GetValue<GIF::Quat>("qIM").y();
      odometryMsg_.pose.pose.orientation.z = filter_.GetState().GetValue<GIF::Quat>("qIM").z();
  //    for(unsigned int i=0;i<6;i++){
  //      unsigned int ind1 = mtOutput::template getId<mtOutput::_pos>()+i;
  //      if(i>=3) ind1 = mtOutput::template getId<mtOutput::_att>()+i-3;
  //      for(unsigned int j=0;j<6;j++){
  //        unsigned int ind2 = mtOutput::template getId<mtOutput::_pos>()+j;
  //        if(j>=3) ind2 = mtOutput::template getId<mtOutput::_att>()+j-3;
  //        odometryMsg_.pose.covariance[j+6*i] = imuOutputCov_(ind1,ind2);
  //      }
  //    }
      odometryMsg_.twist.twist.linear.x = filter_.GetState().GetValue<GIF::Vec3>("MvM")(0);
      odometryMsg_.twist.twist.linear.y = filter_.GetState().GetValue<GIF::Vec3>("MvM")(1);
      odometryMsg_.twist.twist.linear.z = filter_.GetState().GetValue<GIF::Vec3>("MvM")(2);
      odometryMsg_.twist.twist.angular.x = filter_.GetState().GetValue<GIF::Vec3>("MwM")(0);
      odometryMsg_.twist.twist.angular.y = filter_.GetState().GetValue<GIF::Vec3>("MwM")(1);
      odometryMsg_.twist.twist.angular.z = filter_.GetState().GetValue<GIF::Vec3>("MwM")(2);
  //    for(unsigned int i=0;i<6;i++){
  //      unsigned int ind1 = mtOutput::template getId<mtOutput::_vel>()+i;
  //      if(i>=3) ind1 = mtOutput::template getId<mtOutput::_ror>()+i-3;
  //      for(unsigned int j=0;j<6;j++){
  //        unsigned int ind2 = mtOutput::template getId<mtOutput::_vel>()+j;
  //        if(j>=3) ind2 = mtOutput::template getId<mtOutput::_ror>()+j-3;
  //        odometryMsg_.twist.covariance[j+6*i] = imuOutputCov_(ind1,ind2);
  //      }
  //    }
//      std::cout << filter_.GetState().GetValue<GIF::Vec3>("IrIJ").transpose() << std::endl;
//      std::cout << filter_.GetState().GetValue<GIF::Quat>("qIJ") << std::endl;
//      const int IrIJ_start = filter_.GetState().GetStart(filter_.GetState().FindName("IrIJ"));
//      std::cout << filter_.GetCovariance().block(IrIJ_start,IrIJ_start,3,3) << std::endl;
      pubOdometry_.publish(odometryMsg_);
    }
  }

};

int main(int argc, char** argv){
  google::InitGoogleLogging(argv[0]);
  ros::init(argc, argv, "Handler");
  ros::NodeHandle nh;
  DynamicStateEstimatorNode node(nh);
  ros::spin();
  return 0;
}
