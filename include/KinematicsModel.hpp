/*
 * KinematicsModel.hpp
 *
 *  Created on: Oct 21, 2015
 *      Author: Dario Bellicoso
 */

#pragma once

#include <quadruped_model/quadruped_model.hpp>
#include <quadruped_model/QuadrupedState.hpp>
#include <quadruped_model/common/rbdl_utils.hpp>
#include "generalized_information_filter/common.h"
#include "generalized_information_filter/residuals/kinematics-model.h"

class KinematicsModel : public quadruped_model::QuadrupedModel, public GIF::LeggedRobotModel<4,3>
{
 public:
  KinematicsModel(double dt = 0.0025): quadruped_model::QuadrupedModel(dt){};
  virtual ~KinematicsModel(){};

  // TODO: cleanup function parameters, clarify wether model is changes
  Eigen::Vector3d forwardKinematicsBaseToFootInBaseFrame(Eigen::Vector3d angles,unsigned int legId){
    quadruped_model::QuadrupedState quadrupedState;
    quadruped_model::JointPositions jointPositions;
    jointPositions.toImplementation().segment<3>(legId * 3) = angles;
    quadrupedState.setJointPositions(jointPositions);
    setState(quadrupedState);
    return getPositionBodyToBody(quadruped_model::BodyEnum::BASE, getBranchEnumFromLimbUInt(legId),
                                 quadruped_model::BodyNodeEnum::FOOT,
                                 quadruped_model::CoordinateFrame::BASE);
  }
  Eigen::Matrix3d getJacobianTranslationBaseToSegment(Eigen::Vector3d angles,unsigned int legId,unsigned int segmentId){
    quadruped_model::QuadrupedState quadrupedState;
    quadruped_model::JointPositions jointPositions;
    jointPositions.toImplementation().block<3, 1>(legId * 3, 0) = angles;
    quadrupedState.setJointPositions(jointPositions);
    setState(quadrupedState);
    Eigen::MatrixXd jacobian = Eigen::MatrixXd::Zero(3, getDofCount());
    getJacobianTranslationWorldToBody(jacobian, getBranchEnumFromLimbUInt(legId),
                                      quadruped_model::BodyNodeEnum::FOOT,
                                      quadruped_model::CoordinateFrame::BASE);

    return jacobian.block<3, 3>(0, 6 + legId * 3);
  }
  Eigen::Vector3d forwardKinematicsWorldToFootInWorldFrame(Eigen::Vector3d angles,unsigned int legId){
    quadruped_model::QuadrupedState quadrupedState;
    quadruped_model::JointPositions jointPositions;
    jointPositions.toImplementation().segment<3>(legId * 3) = angles;
    quadrupedState.setJointPositions(jointPositions);
    setState(quadrupedState);
    GIF::Vec3 pos;
    getPositionWorldToBody(pos, getBranchEnumFromLimbUInt(legId),
                                 quadruped_model::BodyNodeEnum::FOOT,
                                 quadruped_model::CoordinateFrame::WORLD);
    return pos;
  }
  Eigen::Matrix<double,3,18> getJacobianTranslationWorldToSegment(unsigned int legId,unsigned int segmentId){
    Eigen::MatrixXd jacobian = Eigen::MatrixXd::Zero(3, getDofCount());
    getJacobianTranslationWorldToBody(jacobian, getBranchEnumFromLimbUInt(legId),
                                      quadruped_model::BodyNodeEnum::FOOT,
                                      quadruped_model::CoordinateFrame::WORLD);

    return jacobian;
  }
  void setGeneralizedPositions(const GIF::Vec3& IrIB, const GIF::Quat& qIB,
                               const std::array<GIF::Vec3,4>& enc){
    setGeneralizedPositionsState(IrIB,qIB,enc);
  }
  GIF::VecX computeGeneralizedPositions(const GIF::Vec3& IrIB, const GIF::Quat& qIB,
                                   const std::array<GIF::Vec3,4>& enc){
    setGeneralizedPositionsState(IrIB,qIB,enc);
    return stateGeneralizedPositionsQuaternionRBDL_;
  }
  void setGeneralizedVelocities(const GIF::Vec3& IvM, const GIF::Vec3& MwM,
                                const std::array<GIF::Vec3,4>& end){
    setGeneralizedVelocitiesState(IvM,MwM,end);
  }
  GIF::VecX computeGeneralizedVelocities(const GIF::Vec3& IvM, const GIF::Vec3& MwM,
                                         const std::array<GIF::Vec3,4>& end){
    setGeneralizedVelocitiesState(IvM,MwM,end);
    return stateGeneralizedVelocitiesAngularRBDL_;
  }
  virtual bool getMassInertiaMatrix(Eigen::MatrixXd& M){
    return quadruped_model::QuadrupedModel::getMassInertiaMatrix(M);

  }
  virtual bool getNonlinearEffects(Eigen::VectorXd& h){
    return quadruped_model::QuadrupedModel::getNonlinearEffects(h);
  }
  virtual bool getSelectionMatrix(Eigen::MatrixXd& S){
    S = getActuatorSelectionMatrix();
    return true;
  }
  void setMass(double m){
    auto body = bodyContainer_[BodyEnum::BASE];
    body.setMass(m); // 20 kg
    body.updateInertiaProperties();
  }
  void setMassOffset(const GIF::Vec3& offset){
    auto body = bodyContainer_[BodyEnum::BASE];
    body.setPositionBodyToBodyCom(offset,CoordinateFrame::BODY);
    body.updateInertiaProperties();
  }

 private:

  void setGeneralizedPositionsState(const GIF::Vec3& IrIB, const GIF::Quat& qIB,
                                    const std::array<GIF::Vec3,4>& enc){
    state_.setPositionWorldToBaseInWorldFrame(kindr::Position3D(IrIB));
    state_.setOrientationBaseToWorld(qIB);
    quadruped_model::JointPositions jointPositions;
    for(int l=0;l<4;l++){
      for(int i=0;i<3;i++){
        jointPositions(l*3+i) = enc[l](i);
      }
    }
    state_.setJointPositions(jointPositions);
    setRbdlQFromState(stateGeneralizedPositionsQuaternionRBDL_, state_, true);
  }
  void setGeneralizedVelocitiesState(const GIF::Vec3& IvM, const GIF::Vec3& MwM,
                                const std::array<GIF::Vec3,4>& end){
    state_.setAngularVelocityBaseInBaseFrame(kindr::LocalAngularVelocityPD(MwM));
    state_.setLinearVelocityBaseInWorldFrame(kindr::Velocity3D(IvM));
    quadruped_model::JointVelocities jointVelocities;
    for(int l=0;l<4;l++){
      for(int i=0;i<3;i++){
        jointVelocities(l*3+i) = end[l](i);
      }
    }
    state_.setJointVelocities(jointVelocities);
    setRbdlQDotFromState(stateGeneralizedVelocitiesAngularRBDL_, state_, true);
  }
};


