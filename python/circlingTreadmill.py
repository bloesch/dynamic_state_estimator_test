# Imports
import os, sys, inspect
from pylab import *

ttk_folder = '/home/michael/workspace/trajectory_toolkit/src/trajectory_toolkit'
if ttk_folder not in sys.path:
    sys.path.insert(0, ttk_folder)

import Utils
from TimedData import TimedData
from Plotter import Plotter
from Plotter2D import Plotter2D
from VIEvaluator import VIEvaluator
from matplotlib.patches import Rectangle

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'

# A couple of variables
td_odom = TimedData()
td_mocap = TimedData()
evaluator = VIEvaluator()
td_odom_2 = TimedData()
td_mocap_2 = TimedData()
evaluator2 = VIEvaluator()

figureOutputFolder = '/home/michael'

plotRon = False
plotAtt = False
plotPos = False
plotVel = True
plotRor = True
plotYpr = True

######################################### Evaluation of legged odometry #########################################
testFolder = '/home/michael/rbdl_ws'
Utils.createFolderIfMissing(testFolder)
# bag = '/home/michael/workspace/dynamic_state_estimator_test/2016-10-06-16-37-08.bag' # old setup
bag = '/home/michael/workspace/dynamic_state_estimator_test/2016-11-14-11-05-34.bag'

evaluator.bag = bag
evaluator.odomTopic = '/odometry'
evaluator.gtFile = bag
evaluator.gtTopic = '/starleth/pose'
evaluator.minDt = 0.01
evaluator.startcut = 0
evaluator.endcut = 0
evaluator.derMode = 1
evaluator.alignMode = 0
evaluator.doCov = False
evaluator.doNFeatures = 0
evaluator.doExtrinsics = False
evaluator.doBiases = False
evaluator.plotLeutiDistances = []
evaluator.initTimedData(td_odom)
evaluator.initTimedDataGT(td_mocap)
evaluator.acquireData()
evaluator.acquireDataGT()
evaluator.getAllDerivatives()
evaluator.alignTime()
evaluator.alignBodyFrame()
evaluator.alignInertialFrame([1,2,3,4,5,6])
evaluator.getYpr()

colorsOdom = [Utils.colors['blue'],Utils.colors['blue'],Utils.colors['blue']]
colorsMocap = [Utils.colors['green'],Utils.colors['green'],Utils.colors['green']]

if plotPos: # Position plotting
    plotterPos = Plotter(-1, [3,1],'Estimated Position',['','','Time[s]'],['x[m]','y[m]','z[m]'],10000)
    plotterPos.legendLoc = 2
    plotterPos.addDataToSubplotMultiple(td_odom, 'pos', [1,2,3], colorsOdom, ['Odom','',''])
    plotterPos.addDataToSubplotMultiple(td_mocap, 'pos', [1,2,3], colorsMocap, ['Groundtruth','',''])
 
if plotVel: # Velocity plotting
    plotterVel = Plotter(-1, [3,1],'Estimated Robocentric Velocity',['','','Time[s]'],['v\_x[m/s]','v\_y[m/s]','v\_z[m/s]'],10000)
    plotterVel.legendLoc = 2
    plotterVel.addDataToSubplotMultiple(td_odom, 'vel', [1,2,3], colorsOdom, ['Odom','',''])
    plotterVel.addDataToSubplotMultiple(td_mocap, 'vel', [1,2,3], colorsMocap, ['Groundtruth','',''])
 
if plotRor: # Rotation rate plotting
    plotterRor = Plotter(-1, [3,1],'Estimated Rotational Rate',['','','Time[s]'],['w\_x[m/s]','w\_y[m/s]','w\_z[m/s]'],10000)
    plotterRor.legendLoc = 2
    plotterRor.addDataToSubplotMultiple(td_odom, 'ror', [1,2,3], colorsOdom, ['Odom','',''])
    plotterRor.addDataToSubplotMultiple(td_mocap, 'ror', [1,2,3], colorsMocap, ['Groundtruth','',''])
     
if plotAtt: # Attitude plotting
    plotterAtt = Plotter(-1, [4,1],'Estimated Attitude Quaternion',['','','','Time[s]'],['w[1]','x[1]','y[1]','z[1]'],10000)
    plotterAtt.addDataToSubplotMultiple(td_odom, 'att', [1,2,3,4], ['r','r','r','r'], ['Odom','','',''])
    plotterAtt.addDataToSubplotMultiple(td_mocap, 'att', [1,2,3,4], ['b','b','b','b'], ['Groundtruth','','',''])
 
if plotYpr: # Yaw-pitch-roll plotting
    plotterYpr = Plotter(-1, [3,1],'Estimated Attitude',['','','Time[s]'],['roll[rad]','pitch[rad]','yaw[rad]'],10000)
    plotterYpr.legendLoc = 2
    plotterYpr.addDataToSubplotMultiple(td_odom, 'ypr', [1,2,3], colorsOdom, ['Odom','',''])
    plotterYpr.addDataToSubplotMultiple(td_mocap, 'ypr', [1,2,3], colorsMocap, ['Groundtruth','',''])

plotter2D = Plotter2D(-1,'Top View - Legged Odometry','x[m]','y[m]',10000)
plotter2D.legendLoc = 2
plotter2D.addDataToSubplot(td_odom, td_odom.getColIDs('pos')[0], td_odom.getColIDs('pos')[1], colorsOdom[0], 'Regular')
plotter2D.addDataToSubplot(td_mocap, td_mocap.getColIDs('pos')[0], td_mocap.getColIDs('pos')[1], colorsMocap[0], 'Groundtruth')
plotter2D.setGrid()
axes = plotter2D.getAxes()
axes.add_patch(Rectangle((-1.92, -3.12), 1.92, 3.12, 0.0, color=Utils.colors['gray']))
axes.add_patch(Rectangle((-1.86, -3.06), 1.8, 3.0, 0.0, color=Utils.colors['lightgray']))
plt.plot(td_odom.d[0,td_odom.getColIDs('pos')[0]],td_odom.d[0,td_odom.getColIDs('pos')[1]],'*',markersize=20)
plt.axis('equal')
plotter2D.setFigureSize(6,8)


######################################### Evaluation of external pose feedback #########################################
testFolder = '/home/michael/rbdl_ws'
Utils.createFolderIfMissing(testFolder)
# bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-10-06-15-53-35.bag' # old setup, with pose
# bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-11-14-14-00-13.bag' # integrated all int same residual
# bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-11-14-14-48-09.bag' # changed residual to binary
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-11-14-16-13-34.bag' # omega in state
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-11-24-18-56-48.bag' # dynamics used
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-09-11-20-52.bag' # splitted residuals
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-09-14-07-23.bag' # splitted residuals with dynamics w=1
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-09-14-52-15.bag' # splitted residuals with dynamics w=1e1
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-13-08-59-49.bag' # splitted residuals with dynamics w=1e2
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-13-10-44-59.bag' # QR
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-13-10-54-39.bag' # no imu
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-13-11-01-09.bag' # no imu, w=1e3
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-13-12-59-09.bag' # QR, w=1e3
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-13-17-01-04.bag' # fixed ror dependency
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-13-17-05-10.bag' # fixed ror dependency, no imu
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-14-17-47-13.bag' # new rbdl
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-14-17-53-47.bag' # new rbdl, no imu
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-14-17-58-21.bag' # with mass and offset estimation
bag2 = '/home/michael/workspace/dynamic_state_estimator_test/2016-12-15-09-40-04.bag' # with mass and offset estimation, no imu

evaluator2.bag = bag2
evaluator2.odomTopic = '/odometry'
evaluator2.gtFile = bag2
evaluator2.gtTopic = '/starleth/pose'
evaluator2.minDt = 0.01
evaluator2.startcut = 0
evaluator2.endcut = 0
evaluator2.derMode = 0
evaluator2.alignMode = 0
evaluator2.doCov = False
evaluator2.doNFeatures = 0
evaluator2.doExtrinsics = False
evaluator2.doBiases = False
evaluator2.plotLeutiDistances = []
evaluator2.initTimedData(td_odom_2)
evaluator2.initTimedDataGT(td_mocap_2)
evaluator2.acquireData()
evaluator2.acquireDataGT()
evaluator2.getAllDerivatives()
evaluator2.alignTime()
evaluator2.alignBodyFrame()
evaluator2.alignInertialFrame([1,2,3,4,5,6])
evaluator2.getYpr()

colorsOdom2 = [Utils.colors['red'],Utils.colors['red'],Utils.colors['red']]

if plotPos: # Position plotting
    plotterPos.addDataToSubplotMultiple(td_odom_2, 'pos', [1,2,3], colorsOdom2, ['Odom','',''])
 
if plotVel: # Velocity plotting
    plotterVel.addDataToSubplotMultiple(td_odom_2, 'vel', [1,2,3], colorsOdom2, ['Odom','',''])
 
if plotRor: # Velocity plotting
    plotterRor.addDataToSubplotMultiple(td_odom_2, 'ror', [1,2,3], colorsOdom2, ['Odom','',''])
     
if plotAtt: # Attitude plotting
    plotterAtt.addDataToSubplotMultiple(td_odom_2, 'att', [1,2,3,4], ['r','r','r','r'], ['Odom','','',''])
 
if plotYpr: # Yaw-pitch-roll plotting
    plotterYpr.addDataToSubplotMultiple(td_odom_2, 'ypr', [1,2,3], colorsOdom2, ['Odom','',''])

plotter2D.addDataToSubplot(td_odom_2, td_odom_2.getColIDs('pos')[0], td_odom_2.getColIDs('pos')[1], colorsOdom2[0], 'With pose')

    
k = raw_input("Should the plots be saved? y? ")
if k=='y':
    figure(plotter2D.figureId)
    plt.savefig(figureOutputFolder + '/res_legged_odometry.eps')
    figure(plotter2D_2.figureId)
    plt.savefig(figureOutputFolder + '/res_legged_odometry_with_pose.eps')
 
     